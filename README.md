# My Zellij layouts

It's a repository to store my [zellij](https://zellij.dev/) layouts.
To use a layout, download the kdl file or copy/paste the content in a kdl file.
This file must be stored in the zellij config folder:

```
~/.config/zellij/layouts
```